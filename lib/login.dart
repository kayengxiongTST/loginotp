import 'package:flutter/material.dart';
import 'package:login_otp/otp.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Login> {
  get controller => null;

  @override
  Widget build(BuildContext context) {
    var _controller = TextEditingController();
    // var materialStateProperty = MaterialStateProperty;
    return Scaffold(
      // appBar: AppBar(
      //   title:
      //     Text(
      //       'Hello World',
      //     ),
      // backgroundColor: Colors.pink,
      // ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: 600,
              padding: EdgeInsets.all(10.10),
              child: TextField(
                keyboardType: TextInputType.number,

                onChanged: (text) {
                  print('TextNumber:  $text');
                },
                controller: _controller,
                decoration: InputDecoration(
                  hintText: 'Number',
                  suffixIcon: IconButton(
                    onPressed: _controller.clear,
                    icon: Icon(Icons.clear),
                  ),
                  prefixIcon: Container(
                    width: 100,
                    padding: EdgeInsets.all(10.10),
                    child: Row(children: [
                      Container(
                        // padding: EdgeInsets.all(20.0),
                        width: 30,
                        child: Image.network(
                            'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Flag_of_Laos.svg/1200px-Flag_of_Laos.svg.png'),
                      ),
                      Text('+856'),
                    ]),
                  ),
                  border: OutlineInputBorder(),
                ),
                // labelText: 'NumberPhone'),
              )),
          Container(
              width: 500,
              padding: EdgeInsets.all(10.10),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.pink),
                ),
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => OTP()))
                },
                child: Text('SEND OTP'),
              ))
        ],
      ),
    );
  }
}
