import 'package:flutter/material.dart';

class OTP extends StatefulWidget {
  const OTP({Key? key}) : super(key: key);

  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  get myController => null;

  late FocusNode firstTextField;
  late FocusNode secondTextField;
  late FocusNode thirdTextField;
  late FocusNode forthTextField;
  late FocusNode fifthTextField;
  late FocusNode sixthTextField;
  late FocusNode textText;
  String theText = '';

  @override
  void initState() {
    super.initState();
    firstTextField = FocusNode();
    secondTextField = FocusNode();
    thirdTextField = FocusNode();
    forthTextField = FocusNode();
    fifthTextField = FocusNode();
    sixthTextField = FocusNode();
    textText = FocusNode();
    firstTextField.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            actions: [Icon(Icons.contact_support_sharp)],
            backgroundColor: Colors.pink),
        body: Column(
          children: [
            Container(
              child: Text(
                'Verify your',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.20),
              child: Text(
                'Phone Number',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      focusNode: firstTextField,
                      style: TextStyle(fontSize: 24.0),
                      onChanged: (text) {
                        print('Text1:  $text');
                        if (text.length > 0) {
                          // secondTextField.requestFocus();
                          FocusScope.of(context).requestFocus(secondTextField);
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24.0),
                      focusNode: secondTextField,
                      onChanged: (text) {
                        print('Text2:  $text');
                        if (text.length > 0) {
                          // thirdTextField.requestFocus();
                          FocusScope.of(context).requestFocus(thirdTextField);
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      focusNode: thirdTextField,
                      style: TextStyle(fontSize: 24.0),
                      onChanged: (text) {
                        print('Text3:  $text');
                        if (text.length > 0) {
                          // forthTextField.requestFocus();
                          FocusScope.of(context).requestFocus(forthTextField);
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      focusNode: forthTextField,
                      style: TextStyle(fontSize: 24.0),
                      onChanged: (text) {
                        print('Text4:  $text');
                        if (text.length > 0) {
                          // fifthTextField.requestFocus();
                          FocusScope.of(context).requestFocus(fifthTextField);
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      focusNode: fifthTextField,
                      style: TextStyle(fontSize: 24.0),
                      onChanged: (text) {
                        print('Text5: $text');
                        if (text.length > 0) {
                          // sixthTextField.requestFocus();
                          FocusScope.of(context).requestFocus(sixthTextField);
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
                Container(
                    height: 50,
                    width: 50,
                    child: TextField(
                      textAlign: TextAlign.center,
                      focusNode: sixthTextField,
                      style: TextStyle(fontSize: 24.0),
                      onChanged: (text) {
                        // Navigator.push(context,
                        // //     MaterialPageRoute(builder: (context) => HomePage()));
                        //       print('Text6:  $text');
                        if (text.length > 0) {
                          // secondTextField.requestFocus();
                          FocusScope.of(context).requestFocus(textText);
                          setState(() {
                            theText = 'Hello Kayeng siab dub';
                          });
                        }
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.pink)),
              ],
            )),
            Container(height: 50, width: 50, child: Text(theText))
          ],
        ));
  }
}
